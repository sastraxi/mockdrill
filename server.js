const compress = require('compression');
const morgan = require('morgan');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const _ = require('lodash');

const root = '.';

/**
 * This is a mock mandrill service that runs on port 6927 by default.
 */

const app = express();
app.use(bodyParser.json()); 
app.use(cookieParser());

const OK = 200;
const BAD_REQUEST = 400;
const NOT_AUTHORIZED = 401;

app.post('/api/1.0/messages/send-template.json', (req, res) => {
  const apiKey = req.body.key;
  const templateName = req.body.template_name;
  const recipients = req.body.message.to.map(o => o.email);

  const globalMergeVars = {};
  req.body.message.global_merge_vars.forEach(({name, content}) => {
    globalMergeVars[name] = content;
  });

  console.log("");
  console.log("Message: " + templateName);
  console.log("To: " + recipients.join(', '));
  for (key in globalMergeVars) {
    console.log(key + ": " + globalMergeVars[key]);
  }

  res.status(OK).send(recipients.map(email => { return {
    email: email,
    status: "sent",
    _id: "abc123abc123abc123abc123abc123"
  }}));
});

app.get('/', (req, res) => {
  res.type('text/html');
  res.status(OK);
  res.send('Mockdrill: try a mandrill API call');
});

app.set('port', process.env.PORT || 6927);
const server = app.listen(app.get('port'), () => {
  console.log(`Mockdrill listening on ${server.address().port} (HTTP)`);
});
