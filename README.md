Mockdrill
=========

Mockdrill exposes a Mandrill-compatible API as well as ways to query what mail has been sent through it.

Make sure to set the content-type for JSON POST requests (application/json).
